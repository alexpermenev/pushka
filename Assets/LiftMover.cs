﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LiftMover : MonoBehaviour
{
    public Animator lift;

    private void OnTriggerEnter(Collider other)
    {
        if (!other.CompareTag("Player")) return;

        lift.Play("lift");
    }
}
