﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MenuController : MonoBehaviour
{
    public GameObject panelGame;
    public GameObject panelWinLose;
    public Text textWinLose;
    public Text textScore;
    public GameState gameState;
    public CameraController cameraController;
    public GameObject startGameObject;
    public Pushka pushka;
    public BoolShit boolShit;

    public Animator animator;

    public void OpenWin()
    {
        OpenWinLose("WIN");
        animator.Play("lift");
    }

    public void OpenLose()
    {
        OpenWinLose("LOSE");
    }

    private void OpenWinLose(string title)
    {
        panelGame.SetActive(false);
        panelWinLose.SetActive(true);
        textWinLose.text = title;
        textScore.text = gameState.score.ToString();
    }

    public void OpenGame()
    {
        panelWinLose.SetActive(false);
        panelGame.SetActive(true);
    }

    public void RestartGame()
    {
        OpenGame();
        cameraController.Fly(startGameObject);
        gameState.score = 0;
        pushka.Restart();
        boolShit.Restart();
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            RestartGame();
        }
    }
}
