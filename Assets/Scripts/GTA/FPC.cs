﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPC : MonoBehaviour
{
    private static FPC fpc;

    [SerializeField] private float speed;
    [SerializeField] private float jumpForce;

    private Rigidbody rb;
    private float shift;

    private void Start()
    {
        if (fpc != null)
        {
            Destroy(gameObject);
            return;
        }

        fpc = this;
        DontDestroyOnLoad(gameObject);
        rb = GetComponent<Rigidbody>();
        shift = 1;
    }

    void FixedUpdate()
    {
        if (Input.GetKey(KeyCode.LeftShift))
        {
            shift = 2;
        }
        else
        {
            shift = 1;
        }

        var vert = Input.GetAxis("Vertical");
        var hor = Input.GetAxis("Horizontal");
        rb.AddForce(transform.forward * vert, ForceMode.Impulse);
        rb.AddForce(transform.right * hor, ForceMode.Impulse);

        if (Input.GetKeyDown(KeyCode.Space))
        {
            RaycastHit hit;
            if (Physics.Raycast(transform.position, transform.up * -1, out hit, 1.5f))
            {
                rb.AddForce(transform.up * jumpForce, ForceMode.Impulse);
            }
        }

        Vector3 clampVel = new Vector3(rb.velocity.x, 0, rb.velocity.z);
        rb.velocity = Vector3.ClampMagnitude(clampVel, speed * shift) + new Vector3(0, rb.velocity.y, 0);
    }
}
