﻿using UnityEngine;

public class Wall
{
    public GameObject Trigger;
    public GameObject UpperWall;
    public GameObject LowerWall;
}
